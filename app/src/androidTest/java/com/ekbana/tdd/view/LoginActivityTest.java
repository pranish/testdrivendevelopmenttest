package com.ekbana.tdd.view;

import android.content.Intent;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.ekbana.tdd.R;
import com.ekbana.tdd.constants.MessageConstants;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by pranishshrestha on 5/15/17.
 */
@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {

    ActivityTestRule<LoginActivity> activityTestRule = new ActivityTestRule<>(LoginActivity.class);

    @Test
    public void checkUsernameEditTextIsDisplayed() {
        activityTestRule.launchActivity(new Intent());
        Espresso.onView(ViewMatchers.withId(R.id.edt_username)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
    }

    @Test
    public void checkErrorMessageIsDisplayedForEmptyData() {
        activityTestRule.launchActivity(new Intent());
        Espresso.onView(ViewMatchers.withId(R.id.btn_login)).check(ViewAssertions.matches(ViewMatchers.isDisplayed())).perform(ViewActions.click());
//        Espresso.onView(ViewMatchers.withText(MessageConstants.INVALID_LOGIN)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
    }

    @Test
    public void checkLoginSuccess() {
        activityTestRule.launchActivity(new Intent());
        Espresso.onView(ViewMatchers.withId(R.id.edt_username)).perform(ViewActions.typeText("pranish"), ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.edt_password)).perform(ViewActions.typeText("pranish100"), ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.btn_login)).check(ViewAssertions.matches(ViewMatchers.isDisplayed())).perform(ViewActions.click());
        Espresso.onView(ViewMatchers.withText("Login Successful")).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
    }

}