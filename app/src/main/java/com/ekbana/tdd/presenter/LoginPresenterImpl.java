package com.ekbana.tdd.presenter;

import android.content.Context;

import com.ekbana.tdd.constants.MessageConstants;
import com.ekbana.tdd.view.LoginView;

/**
 * Created by pranishshrestha on 5/12/17.
 */

public class LoginPresenterImpl implements LoginPresenter {

    private LoginView view;

    private MessageConstants messageConstants;

//    private Context context;

    public static final int MAX_LOGIN_ATTEMPTS = 3;

    public int loginAttempts;

    public LoginPresenterImpl(LoginView view, Context context) {
        this.view = view;
//        this.context = context;
        messageConstants = new MessageConstants(context);
    }

    @Override
    public int increaseLoginAttempt() {
        loginAttempts = loginAttempts + 1;
        return loginAttempts;
    }

    @Override
    public boolean isLoginAttemptExceeded() {
        return loginAttempts >= MAX_LOGIN_ATTEMPTS;
    }

    @Override
    public boolean isLoginSuccess(String username, String password) {
        if (isLoginAttemptExceeded()) {
            view.showMessage(messageConstants.MAXIMUM_LOGIN_ATTEMPTS);
            return false;
        }

        if (username.equals("pranish") && password.equals("pranish100")) {
            view.showMessage(messageConstants.LOGIN_SUCCESS);
            return true;
        }

        increaseLoginAttempt();
        view.showMessage(messageConstants.INVALID_LOGIN);
        return false;
    }

}
