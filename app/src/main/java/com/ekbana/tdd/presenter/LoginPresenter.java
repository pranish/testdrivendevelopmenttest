package com.ekbana.tdd.presenter;

/**
 * Created by pranishshrestha on 5/12/17.
 */

public interface LoginPresenter {

    int increaseLoginAttempt();

    boolean isLoginAttemptExceeded();

    boolean isLoginSuccess(String username, String password);
}
