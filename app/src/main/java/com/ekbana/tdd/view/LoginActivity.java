package com.ekbana.tdd.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ekbana.tdd.R;
import com.ekbana.tdd.presenter.LoginPresenter;
import com.ekbana.tdd.presenter.LoginPresenterImpl;

public class LoginActivity extends AppCompatActivity implements LoginView {

    private LoginPresenter presenter;

    private Button btnSignin;

    private EditText txtUsername, txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializePresenter();
        initializeViews();
        initializeEvents();

    }

    private void initializeViews() {

        txtUsername = (EditText) findViewById(R.id.edt_username);
        txtPassword = (EditText) findViewById(R.id.edt_password);
        btnSignin = (Button) findViewById(R.id.btn_login);

    }

    private void initializePresenter() {
        presenter = new LoginPresenterImpl(this, this);
    }

    private void initializeEvents() {

        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.isLoginSuccess(txtUsername.getText().toString().trim(), txtPassword.getText().toString().trim());
            }
        });

    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
