package com.ekbana.tdd.constants;

import android.content.Context;

import com.ekbana.tdd.R;

/**
 * Created by pranishshrestha on 5/12/17.
 */

public class MessageConstants {

    private Context context;

    public MessageConstants(Context context) {
        this.context = context;
    }

/*    public String INVALID_LOGIN = context.getResources().getString(R.string.invalid_login);

    public String LOGIN_SUCCESS = context.getResources().getString(R.string.login_success);

    public String MAXIMUM_LOGIN_ATTEMPTS = context.getResources().getString(R.string.max_login_attempts);*/

    public static String INVALID_LOGIN = "Invalid Login";

    public static String LOGIN_SUCCESS = "Login Success";

    public static String MAXIMUM_LOGIN_ATTEMPTS = "Maximum login attempts";

}
