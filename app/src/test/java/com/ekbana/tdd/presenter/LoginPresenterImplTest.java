package com.ekbana.tdd.presenter;

import android.content.Context;
import android.test.mock.MockContext;

import com.ekbana.tdd.view.LoginView;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by pranishshrestha on 5/12/17.
 */
public class LoginPresenterImplTest {

    @Test
    public void checkIfLoginAttemptExceeded() {
        LoginView loginView = mock(LoginView.class);
        Context context = mock(Context.class);
        LoginPresenter loginPresenter = new LoginPresenterImpl(loginView, context);
        Assert.assertEquals(1, loginPresenter.increaseLoginAttempt());
        Assert.assertEquals(2, loginPresenter.increaseLoginAttempt());
        Assert.assertEquals(3, loginPresenter.increaseLoginAttempt());
        Assert.assertTrue(loginPresenter.isLoginAttemptExceeded());
    }

    @Test
    public void checkUsernameAndPasswordIsCorrect() {
        LoginView loginView = mock(LoginView.class);
        Context context = new MockContext();
        LoginPresenter loginPresenter = new LoginPresenterImpl(loginView, context);
        Assert.assertTrue(loginPresenter.isLoginSuccess("pranish", "pranish100"));
    }
}